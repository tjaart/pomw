# -*- python-indent-offset: 4; -*-

from datetime import date
from os import mkdir
from os.path import getsize, isdir, isfile, join
from shutil import rmtree

import unijson
from pytest import fixture

from pomw.model import Entries, Entry


@fixture(scope="function")
def tmpdir():
    path = join("tests", "tmp")
    if not isdir(path):
        mkdir(path)
    yield path
    # rmtree(path)


@fixture(scope="module")
def entries():
    yield [
        {"uuid": "3", "date": date(2018, 8, 2), "planned": 1, "pomodoros": []},
        {"uuid": "2", "date": date(2018, 8, 1), "planned": 1, "pomodoros": []},
        {"uuid": "1", "date": date(2018, 8, 1), "planned": 1, "pomodoros": []},
    ]


def test_save(entries, tmpdir):
    entries = Entries(entries=entries, load=False)
    # path = join(tmpdir, '2018-08.json')
    # with open(path, mode='w') as f:
    #     s = unijson.dump(entries, f)
    # assert isfile(path)
    # assert getsize(path) > 0


# def test_load(tmpdir, entries):
#     # my_entries = Entries(path=join('tests', 'fixtures'), year_month=date(2018, 2, 1), entries=entries)
#     path = join('tests', 'fixtures', '2018-02.json')
#     with open(path, mode='r') as f:
#         my_entries = unijson.load(f)
#     assert len(my_entries) == 3
#     print(my_entries[0])
#     assert my_entries[0].date == date(2018, 8, 1)


def test_find(entries):
    my_entries = Entries(entries=entries, load=False)
    found_entries = my_entries.find(uuid="3")
    entry = found_entries[0]
    assert entry.uuid == "3"
    assert entry.date == date(2018, 8, 2)

    result_entries = my_entries.find(date=date(2018, 8, 1))
    # print(result_entries)
    assert result_entries[0].uuid == "1"
    assert result_entries[1].uuid == "2"


def test_insert(entries):
    entries = Entries(entries=entries, load=False)
    entries.insert(Entry(uuid=4, date=date(2018, 2, 1)))
    assert len(entries) == 4


def test_lt():
    assert Entry(uuid="2", date=date(2018, 2, 2)) < Entry(
        uuid="2", date=date(2018, 2, 3)
    )
    assert not Entry(uuid="4", date=date(2018, 2, 4)) < Entry(
        uuid="2", date=date(2018, 2, 4)
    )
    assert Entry(uuid="2", date=date(2018, 2, 1)) < Entry(
        uuid="4", date=date(2018, 2, 1)
    )


def test_eq():
    assert Entry(uuid=2, date=date(2018, 2, 2)) == Entry(uuid=2, date=date(2018, 2, 2))
    assert not Entry(uuid=2, date=date(2018, 2, 2)) == Entry(
        uuid=2, date=date(2018, 2, 3)
    )
