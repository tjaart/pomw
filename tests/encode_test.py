# -*- python-indent-offset: 4; -*-

from datetime import date, datetime
from re import search

import unijson
from pytest import fixture, raises

from pomw.model import Entries, Entry, Interval, Pomodoro


def test_pomodoro():
    # completed = {'key': 'value'}
    pomodoro = Pomodoro(
        start_time=datetime(2018, 11, 21, 7, 43),
        end_time=datetime(2018, 11, 21, 8, 13),
        internal_interrupt=5,
        external_interrupt=3,
    )
    s = unijson.dumps(pomodoro)

    # start_time
    assert (
        '"_Interval__start_time": {"datetime": "2018-11-21 07:43:00.000000", \
"tzinfo": null, "__class__": "datetime", "__module__": "datetime"}'
        in s
    )
    # end_time
    assert (
        '"_Interval__end_time": {"datetime": "2018-11-21 08:13:00.000000", \
"tzinfo": null, "__class__": "datetime", "__module__": "datetime"}'
        in s
    )

    # internal_interrupt
    assert '"_Pomodoro__internal_interrupt": 5' in s

    # external interrupt
    assert '"_Pomodoro__external_interrupt": 3' in s

    # void
    assert '"_Pomodoro__void": false' in s

    with raises(TypeError, match="'end_time' should be of type datetime"):
        Pomodoro(end_time="8:35", internal_interrupt=5, external_interrupt=3)

    with raises(TypeError, match="'internal_interrupt' should be of type int"):
        Pomodoro(
            start_time=datetime(2018, 3, 13, 8, 13),
            internal_interrupt="invalid",
            external_interrupt=3,
        )

    with raises(TypeError, match="'external_interrupt' should be of type int"):
        Pomodoro(
            start_time=datetime(2019, 1, 15, 8, 13),
            internal_interrupt=4,
            external_interrupt=3.547,
        )


def test_interval():
    nonpom = Interval(
        start_time=datetime(2013, 5, 30, 14, 25), end_time=datetime(2013, 5, 30, 15, 12)
    )
    s = unijson.dumps(nonpom)

    # start_time
    assert (
        '{"_Interval__start_time": {"datetime": "2013-05-30 14:25:00.000000", "tzinfo": null, \
"__class__": "datetime", "__module__": "datetime"}'
        in s
    )

    # end_time
    assert (
        '"_Interval__end_time": {"datetime": "2013-05-30 15:12:00.000000", "tzinfo": null, \
"__class__": "datetime", "__module__": "datetime"}'
        in s
    )

    with raises(TypeError, match="'start_time' should be of type datetime"):
        nonpom = Interval(
            start_time="not a time", end_time=datetime(2018, 3, 17, 15, 12)
        )

    with raises(TypeError, match="'end_time' should be of type datetime"):
        nonpom = Interval(start_time=datetime(2018, 3, 17, 15, 12), end_time=3)


def test_entry():
    entry = Entry(
        uuid="123",
        date=date(2018, 2, 3),
        planned=2,
        intervals=[
            Pomodoro(
                end_time=datetime(2018, 2, 3, 8, 13),
                internal_interrupt=5,
                external_interrupt=3,
            )
        ],
    )

    s = unijson.dumps(entry)

    # uuid
    assert '"_Entry__uuid": "123"' in s

    # date
    assert (
        '"_Entry__date": {"date": "2018-02-03", "__class__": "date", "__module__": "datetime"}'
        in s
    )

    # planned
    assert '"_Entry__planned": 2' in s
