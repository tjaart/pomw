# -*- python-indent-offset: 4; -*-

from datetime import date, datetime, time

import unijson


def test_nonpom():
    s = '{"_Interval__start_time": {"datetime": "2018-03-13 14:25:00.000000", \
"tzinfo": null, "__class__": "datetime", "__module__": "datetime"}, "_Interval__end_time": \
{"datetime": "2018-03-13 15:12:00.000000", "tzinfo": null, "__class__": "datetime", "__module__": "datetime"}, \
"__class__": "Interval", "__module__": "pomw.model.interval"}'
    nonpom = unijson.loads(s)
    assert nonpom.start_time == datetime(2018, 3, 13, 14, 25)
    assert nonpom.end_time == datetime(2018, 3, 13, 15, 12)


def test_pomodoro():
    s = '{"_Interval__start_time": {"datetime": "2018-05-21 07:43:00.000000", "tzinfo": null, \
"__class__": "datetime", "__module__": "datetime"}, "_Interval__end_time": {"datetime": \
"2018-05-21 08:13:00.000000", "tzinfo": null, "__class__": "datetime", "__module__": "datetime"}, \
"_Pomodoro__internal_interrupt": 5, "_Pomodoro__external_interrupt": 3, "__class__": "Pomodoro", \
"__module__": "pomw.model.pomodoro"}'

    pomodoro = unijson.loads(s)

    assert pomodoro.start_time == datetime(2018, 5, 21, 7, 43)
    assert pomodoro.end_time == datetime(2018, 5, 21, 8, 13)
    assert pomodoro.internal_interrupt == 5
    assert pomodoro.external_interrupt == 3


def test_entry():
    s = '{"_Entry__uuid": "123", "_Entry__date": {"date": "2018-02-03", "__class__": "date", \
"__module__": "datetime"}, "_Entry__planned": 2, "_Entry__intervals": [{"_Interval__start_time": \
{"datetime": "2018-02-03 08:13:00.000000", "tzinfo": null, "__class__": "datetime", "__module__": "datetime"}, \
"_Pomodoro__internal_interrupt": 5, "_Pomodoro__external_interrupt": 3, "__class__": "Pomodoro", \
"__module__": "pomw.model.pomodoro"}], "__class__": "Entry", "__module__": "pomw.model.entry"}'
    entry = unijson.loads(s)

    assert entry.uuid == "123"
    assert entry.date == date(2018, 2, 3)
    assert entry.planned == 2

    assert entry.intervals[0].internal_interrupt == 5
    assert entry.pomodoros[0].external_interrupt == 3
    assert entry.pomodoros[0].start_time == datetime(2018, 2, 3, 8, 13)
